package algorithms.greedyAlgorithm.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VariousTerms {

    public static void main(String [] args) throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        String s = in.readLine();

        long number = Long.parseLong(s);
        long size = (long) (Math.sqrt(8*number+1)-1)/2;
        long count = 1;


        System.out.println(size);
        while (number != 0) {

            if (number - count  <= count) {
                System.out.println(number);
                break;
            }

            System.out.print(count + " ");
            number -= count;
            count++;
        }

    }

}

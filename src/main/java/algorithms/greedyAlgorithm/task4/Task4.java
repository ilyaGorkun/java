package algorithms.greedyAlgorithm.task4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;


public class Task4 {

    private void run() throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(in.readLine());
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((o1, o2) -> -Integer.compare(o1, o2));
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String [] line = in.readLine().split(" ");
            if (line[0].equals("Insert")) {
                priorityQueue.add(Integer.parseInt(line[1]));
            } else {
                result.add(priorityQueue.poll());
            }
        }

        for (Integer elem: result) {
            System.out.println(elem);
        }


    }

    public static void main(String [] args) throws IOException {
        new Task4().run();
    }

}

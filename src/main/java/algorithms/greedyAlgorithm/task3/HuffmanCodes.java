package algorithms.greedyAlgorithm.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class HuffmanCodes {

    class Node {
        final int sum;
        String code;

        void buildCode(String code) {
            this.code = code;
        }

        public Node(int sum) {
            this.sum = sum;
        }
    }

    class InternalNode extends Node{
        Node left;
        Node right;

        @Override
        void buildCode(String code) {
            super.buildCode(code);
            left.buildCode(code + "0");
            right.buildCode(code + "1");
        }

        public InternalNode(Node left, Node right) {
            super(left.sum + right.sum);
            this.left = left;
            this.right = right;
        }
    }

    class LeafNode extends Node{
        char symbol;

        @Override
        void buildCode(String code) {
            super.buildCode(code);
            System.out.println(symbol + ": " +code);
        }

        public LeafNode(char symbol, int count) {
            super(count);
            this.symbol = symbol;
        }
    }


    private void run() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String  line = in.readLine();
        Map<Character, Integer> count = new HashMap<>();

        for (char elem: line.toCharArray()) {
            if (count.containsKey(elem)) {
                count.put(elem, count.get(elem) +1);
            } else  {
                count.put(elem, 1);
            }
        }

        Map<Character, Node> charNode = new HashMap<>();
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>(Comparator.comparingInt(o -> o.sum));

        for(Map.Entry<Character, Integer> elem: count.entrySet()) {
            LeafNode leafNode =  new LeafNode(elem.getKey(), elem.getValue());
            charNode.put(elem.getKey(), leafNode);
            priorityQueue.add(leafNode);

        }

        int sum = 0;
        while (priorityQueue.size() > 1) {
            Node first = priorityQueue.poll();
            Node second  = priorityQueue.poll();
            InternalNode node = new InternalNode(first, second);
            sum += node.sum;
            priorityQueue.add(node);
        }

        if(count.size() == 1) {
            sum = line.length();
        }

        Node root = priorityQueue.peek();
        System.out.println(count.size() + " " + sum);
        if (count.size() == 1) {
            root.buildCode("0");
        } else {
            root.buildCode("");
        }
        StringBuilder result = new StringBuilder();
        for (int i=0; i < line.length(); i++) {
            result.append(charNode.get(line.charAt(i)).code);
        }

        System.out.println(result.toString());

    }

    public static void main(String [] args) throws IOException {
        new HuffmanCodes().run();
    }

}

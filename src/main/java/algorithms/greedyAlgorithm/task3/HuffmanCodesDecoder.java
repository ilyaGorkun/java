package algorithms.greedyAlgorithm.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class HuffmanCodesDecoder {


    private void run() throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String [] lineStart  = in.readLine().split(" ");
        int count = Integer.parseInt(lineStart[0]);
        int size  = Integer.parseInt(lineStart[1]);

        Map<Character, String> codeSymbol = new HashMap<>();

        for (int i = 0; i <count; i++) {
            String [] line = in.readLine().split(": ");
            codeSymbol.put(line[0].charAt(0), line[1]);
        }



        String code = in.readLine();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < code.length();) {
            for (Map.Entry<Character, String> elem: codeSymbol.entrySet()) {
                if (code.startsWith(elem.getValue(), i)) {
                    result.append(elem.getKey());
                    i += elem.getValue().length();
                    break;
                }
            }
        }

        System.out.println(result.toString());


    }

    public static void main(String [] args) throws IOException {
        new HuffmanCodesDecoder().run();
    }

}

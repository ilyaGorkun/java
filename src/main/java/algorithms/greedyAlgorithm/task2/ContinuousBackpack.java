package algorithms.greedyAlgorithm.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

public class ContinuousBackpack {

    class Item {
         int volume;
         int cost;

        public Item(int volume, int cost) {
            this.volume = volume;
            this.cost = cost;
        }

    }


    private  void run() throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String [] start = in.readLine().split(" ");

        int size = Integer.parseInt(start[0]);
        int w = Integer.parseInt(start[1]);

        Item[] items  = new Item[size];
        for (int i = 0; i < size; i++) {
            String [] s = in.readLine().split(" ");
            int cost = Integer.parseInt(s[0]);
            int volume = Integer.parseInt(s[1]);
            items[i] = new Item(volume, cost);
        }


        Arrays.sort(items, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {

                long r1 = (long) o1.cost*o2.volume;
                long r2 = (long) o2.cost*o1.volume;
                return -Long.compare(r1, r2);
            }
        });

        double result = 0;
        for (Item elem: items) {
            if (elem.volume <= w) {
                result += elem.cost;
                w  -= elem.volume;
            } else {
                result += (double)elem.cost * w / elem.volume;
                break;
            }
        }

        System.out.println(result);



    }

    public static void main(String [] args) throws IOException {
        new ContinuousBackpack().run();
    }

}

package algorithms.divideAndConquer.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BinarySearch {


    private  int binarySearch(int elem, int [] arr) {

        int left = 0;
        int right = arr.length -1;

        while(right >= left) {
            int middle = (left+right) >> 1;

            if (elem == arr[middle]) {
                return middle + 1;
            } else if (arr[middle] > elem) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }

        return -1;
    }

    private void run() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        String [] line1 = in.readLine().split(" ");
        int sizeArr1 = Integer.parseInt(line1[0]);
        int [] arrSort = new int[sizeArr1];
        for (int i = 0; i < sizeArr1; i++) {
            arrSort[i] = Integer.parseInt(line1[i+1]);
        }

        String [] line2 = in.readLine().split(" ");
        int sizeArr2 = Integer.parseInt(line2[0]);
        int [] arr = new int[sizeArr2];
        for (int i = 0; i < sizeArr2; i++) {
            arr[i] = Integer.parseInt(line2[i+1]);
        }

        for (int i = 0; i< sizeArr2; i++) {
            System.out.print(binarySearch(arr[i],arrSort) + " ");
        }

    }


    public static void main(String [] args) throws IOException {
        new BinarySearch().run();
    }


}

package algorithms.divideAndConquer.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class InverseСounting {

    long count = 0;
    private int [] temp;
    private int [] a;

    private void merge(int l, int m, int r) {
        int i = l;
        int j = m;

        for (int k = l; k < r; k++) {
            if (j == r || (i < m && a[i] <= a[j])) {
                temp[k] = a[i];
                i++;
            } else {
                count += m - i;
                temp[k] = a[j];
                j++;
            }
        }
        System.arraycopy(temp, l, a,l,r -  l);
    }

    private void mergeSort(int l, int r) {
        if (r <= l+1) {
            return;
        }
        int m = (l+r) >> 1;
        mergeSort(l, m);
        mergeSort(m, r);
        merge(l, m, r);

    }

    private void run() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));


        int size = Integer.parseInt(in.readLine());
        String [] line  = in.readLine().split(" ");
        a = new int[size];

        for (int i = 0; i < size; i++) {
            a[i] = Integer.parseInt(line[i]);
        }
        temp = new int[size];
        mergeSort(0,size);
        System.out.println(count);

    }

    public static void main(String [] args) throws IOException {
        new InverseСounting().run();
    }

}

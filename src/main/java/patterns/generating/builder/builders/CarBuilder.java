package patterns.generating.builder.builders;

import patterns.generating.builder.buildersimpl.Builder;
import patterns.generating.builder.cars.Car;
import patterns.generating.builder.cars.Type;
import patterns.generating.builder.components.Engine;
import patterns.generating.builder.components.GPSNavigator;
import patterns.generating.builder.components.Transmission;
import patterns.generating.builder.components.TripComputer;

public class CarBuilder implements Builder {

    private Type type;
    private int seats;
    private Engine engine;
    private Transmission transmission;
    private TripComputer tripComputer;
    private GPSNavigator gpsNavigator;



    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Override
    public void setTripComputer(TripComputer tripComputer) {
        this.tripComputer = tripComputer;
    }

    @Override
    public void setGPSNavigator(GPSNavigator gpsNavigator) {
        this.gpsNavigator = gpsNavigator;
    }

    public Car getResult() {
        return new Car(type,seats, engine, transmission, tripComputer, gpsNavigator);
    }

}

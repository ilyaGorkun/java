package patterns.generating.builder.app;

import patterns.generating.builder.builders.CarBuilder;
import patterns.generating.builder.builders.CarManualBuilder;
import patterns.generating.builder.cars.Car;
import patterns.generating.builder.cars.Manual;
import patterns.generating.builder.director.Director;

public class Demo {

    public static void main(String[] args) {
        Director director = new Director();


        CarBuilder builder = new CarBuilder();
        director.constructSportsCar(builder);

        Car car = builder.getResult();
        System.out.println("Car built:\n" + car.getType());


        CarManualBuilder manualBuilder = new CarManualBuilder();


        director.constructSportsCar(manualBuilder);
        Manual carManual = manualBuilder.getResult();
        System.out.println("\nCar manual built:\n" + carManual.toString());
    }

}

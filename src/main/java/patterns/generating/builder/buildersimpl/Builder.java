package patterns.generating.builder.buildersimpl;

import patterns.generating.builder.cars.Type;
import patterns.generating.builder.components.Engine;
import patterns.generating.builder.components.GPSNavigator;
import patterns.generating.builder.components.Transmission;
import patterns.generating.builder.components.TripComputer;

public interface Builder {

    void setType(Type type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setTripComputer(TripComputer tripComputer);
    void setGPSNavigator(GPSNavigator gpsNavigator);

}

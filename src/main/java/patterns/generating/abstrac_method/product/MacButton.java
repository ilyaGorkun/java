package patterns.generating.abstrac_method.product;

import patterns.generating.abstrac_method.productimpl.Button;

public class MacButton implements Button {
    public void render() {
        System.out.println("Test Button for macOS");
    }

    public void onClick() {
        System.out.println("Hello!");
    }
}

package patterns.generating.abstrac_method.productimpl;

public interface Button {
    void render();
    void onClick();
}

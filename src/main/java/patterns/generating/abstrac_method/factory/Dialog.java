package patterns.generating.abstrac_method.factory;

import patterns.generating.abstrac_method.productimpl.Button;

public abstract class Dialog {

    public void renderWindws() {
        Button button = createButton();
        button.render();
    }

    public abstract Button createButton();

}

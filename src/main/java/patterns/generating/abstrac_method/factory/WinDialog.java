package patterns.generating.abstrac_method.factory;

import patterns.generating.abstrac_method.product.WinButton;
import patterns.generating.abstrac_method.productimpl.Button;

public class WinDialog extends Dialog {
    public Button createButton() {
        return new WinButton();
    }
}

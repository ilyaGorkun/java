package patterns.generating.abstrac_method.app;

import patterns.generating.abstrac_method.factory.Dialog;
import patterns.generating.abstrac_method.factory.WinDialog;

public class Demo {
    private static Dialog dialog;

    public static void main(String [] args) {
        config();
        renderWindow();
    }


    public static void config() {
        String conf = "win";
        if (conf.equals("win")) {
            dialog = new WinDialog();
        } else {
            throw new NullPointerException();
        }
    }

    public static void renderWindow() {
        dialog.renderWindws();
    }
}

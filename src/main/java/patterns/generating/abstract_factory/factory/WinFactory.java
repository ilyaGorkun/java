package patterns.generating.abstract_factory.factory;

import patterns.generating.abstract_factory.factoryimpl.GUIFactory;
import patterns.generating.abstract_factory.product.button.WinButton;
import patterns.generating.abstract_factory.product.checkBox.WinCheckBox;
import patterns.generating.abstract_factory.product.radioButton.WinRadioButton;
import patterns.generating.abstract_factory.productimpl.Button;
import patterns.generating.abstract_factory.productimpl.CheckBox;
import patterns.generating.abstract_factory.productimpl.RadioButton;

public class WinFactory implements GUIFactory {
    public Button creatButton() {
        return new WinButton();
    }

    public CheckBox creatCheckBox() {
        return new WinCheckBox();
    }

    public RadioButton creatRadioButton() {
        return new WinRadioButton();
    }

}

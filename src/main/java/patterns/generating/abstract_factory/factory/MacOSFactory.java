package patterns.generating.abstract_factory.factory;

import patterns.generating.abstract_factory.factoryimpl.GUIFactory;
import patterns.generating.abstract_factory.product.button.MacButton;
import patterns.generating.abstract_factory.product.checkBox.MacCheckBox;
import patterns.generating.abstract_factory.product.radioButton.MacOSRadioButton;
import patterns.generating.abstract_factory.productimpl.Button;
import patterns.generating.abstract_factory.productimpl.CheckBox;
import patterns.generating.abstract_factory.productimpl.RadioButton;

public class MacOSFactory implements GUIFactory {
    public Button creatButton() {
        return new MacButton();
    }

    public CheckBox creatCheckBox() {
        return new MacCheckBox();
    }

    public RadioButton creatRadioButton() {
        return new MacOSRadioButton();
    }
}

package patterns.generating.abstract_factory.factoryimpl;

import patterns.generating.abstract_factory.productimpl.Button;
import patterns.generating.abstract_factory.productimpl.CheckBox;
import patterns.generating.abstract_factory.productimpl.RadioButton;

public interface GUIFactory {

    Button creatButton();

    CheckBox creatCheckBox();

    RadioButton creatRadioButton();

}

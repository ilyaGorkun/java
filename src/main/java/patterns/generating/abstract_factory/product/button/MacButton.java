package patterns.generating.abstract_factory.product.button;

import patterns.generating.abstract_factory.productimpl.Button;

public class MacButton implements Button {

    public void paint() {
        System.out.println("You have created MacOSButtton");
    }
}

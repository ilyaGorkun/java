package patterns.generating.abstract_factory.product.radioButton;

import patterns.generating.abstract_factory.productimpl.RadioButton;

public class MacOSRadioButton implements RadioButton {

    public void paint() {
        System.out.println("You have made MacOSRadioButton");
    }
}

package patterns.generating.abstract_factory.product.checkBox;

import patterns.generating.abstract_factory.productimpl.CheckBox;

public class WinCheckBox implements CheckBox {
    public void paint() {
        System.out.println("You have made WinCheckBox");
    }
}

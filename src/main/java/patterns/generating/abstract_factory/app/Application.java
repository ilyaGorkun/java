package patterns.generating.abstract_factory.app;

import patterns.generating.abstract_factory.factoryimpl.GUIFactory;
import patterns.generating.abstract_factory.productimpl.Button;
import patterns.generating.abstract_factory.productimpl.CheckBox;
import patterns.generating.abstract_factory.productimpl.RadioButton;

public class Application {
    private Button button;
    private CheckBox checkBox;
    private RadioButton radioButton;

    public Application(GUIFactory factory) {
        this.button = factory.creatButton();
        this.checkBox = factory.creatCheckBox();
        this.radioButton = factory.creatRadioButton();
    }

    public void paint() {
        button.paint();
        checkBox.paint();
        radioButton.paint();
    }

}

package patterns.generating.abstract_factory.app;

import patterns.generating.abstract_factory.factory.MacOSFactory;
import patterns.generating.abstract_factory.factory.WinFactory;
import patterns.generating.abstract_factory.factoryimpl.GUIFactory;

import java.util.NoSuchElementException;

public class Demo {

    public static Application appConfig() {
        Application app;
        GUIFactory factory;
        String type = "win";

        if(type.equals("macOS")) {
            factory = new MacOSFactory();
            app = new Application(factory);
        } else if(type.equals("win")) {
            factory = new WinFactory();
            app = new Application(factory);
        } else {
            throw new NoSuchElementException();
        }

        return app;

    }

    public static void main(String []args) {
        Application app = appConfig();
        app.paint();
    }

}

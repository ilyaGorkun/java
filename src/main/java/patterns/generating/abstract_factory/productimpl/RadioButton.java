package patterns.generating.abstract_factory.productimpl;

public interface RadioButton {
    void paint();
}

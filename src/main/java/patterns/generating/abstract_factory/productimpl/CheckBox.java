package patterns.generating.abstract_factory.productimpl;

public interface CheckBox {
    void paint();
}

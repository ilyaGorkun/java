package patterns.generating.abstract_factory.productimpl;

public interface Button {

    void paint();

}

package patterns.structural.facade.action;

import patterns.structural.facade.structures.VideoFile;

import java.io.File;

public class AudioMixer {

    public File fix(VideoFile file) {
        System.out.println("AudioMixer: fixing audio...");
        return new File("tmp");
    }

}

package patterns.structural.facade.action;

import patterns.structural.facade.structures.VideoFile;
import patterns.structural.facade.structures.codec.Codec;

public class BitrateReader {

    public static VideoFile read(VideoFile file, Codec codec) {
        System.out.println("BitrateReader: readeing file");
        return file;
    }

    public static VideoFile convert(VideoFile buffer, Codec codec) {
        System.out.println("BitrateReader: writing file");
        return buffer;
    }


}

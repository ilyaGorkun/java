package patterns.structural.facade.factory;

import patterns.structural.facade.structures.VideoFile;
import patterns.structural.facade.structures.codec.Codec;
import patterns.structural.facade.structures.codec.MPEG4ComCodec;
import patterns.structural.facade.structures.codec.OggComCodec;

public class CodecFactory {

    public static Codec exctract(VideoFile videoFile) {
        String type = videoFile.getCodecType();

        if(type.equals("mp4")){
            System.out.println("CodecFactory: extracting mpeg audio");
            return new MPEG4ComCodec();
        } else {
            System.out.println("CodecFactory: extracting ogg audio");
            return new OggComCodec();
        }
    }

}

package patterns.structural.facade;


import patterns.structural.facade.action.AudioMixer;
import patterns.structural.facade.action.BitrateReader;
import patterns.structural.facade.factory.CodecFactory;
import patterns.structural.facade.structures.VideoFile;
import patterns.structural.facade.structures.codec.Codec;
import patterns.structural.facade.structures.codec.MPEG4ComCodec;
import patterns.structural.facade.structures.codec.OggComCodec;

import java.io.File;

public class VideoConversionFacade {

    public File convertVideo(String fileName, String format) {
        System.out.println("VideoConversionFacade: conversion started");
        VideoFile file = new VideoFile(fileName);
        Codec sourceCodec  = CodecFactory.exctract(file);
        Codec destinationCodec;
        if(format.equals("mp4")) {
            destinationCodec = new OggComCodec();
        } else {
            destinationCodec = new MPEG4ComCodec();
        }

        VideoFile buffer = BitrateReader.read(file,sourceCodec);
        VideoFile intermediateResult = BitrateReader.convert(buffer, destinationCodec);
        File result = new AudioMixer().fix(intermediateResult);
        System.out.println("VideoConversionFacade: conversion completed");
        return result;
    }

}

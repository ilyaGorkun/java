package patterns.structural.facade.app;

import patterns.structural.facade.VideoConversionFacade;

import java.io.File;

public class Demo {

    public static void main(String [] arges ) {
        VideoConversionFacade converter = new VideoConversionFacade();
        File mp4Vide0 = converter.convertVideo("myVideo.ogg","mp4" );
    }
}
